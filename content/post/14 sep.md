#Donderdag, 14 september


Mijn dag begon met mijn eerste workshop over photoshop, ik heb hier niet goed over nagedacht. Ik heb photoshop al 4 jaar gehad op mijn vorige opleiding dus ik ging er met de instelling heen dat ik mijn photoshop skills kan verbeteren, maar er zijn natuurlijk mensen die helemaal geen verstand hebben van dit progamma en daar is deze workshop meer voor bedoelt. Nadat ik mijn opdracht binnen 2 minuutjes afhad ben ik verder gegaan aan mijn persoonlijke opdracht het visueel maken van het onderzoek om deze morgen te kunnen inleveren. 

Het werkcollege ging vandaag door op het hoorcollege van verbeelden. Tijdens dit werkcollege was het de bedoeling om beeld te creëren bij de theorie, weer is tijdens het werkcollege het hoorcollege voor mij duidelijker geworden. Door zelf de termen toe te voegen aan de beelden krijg je er een veel beter begrip van. Ik heb ook nog feedback gekregen of mijn keuzes van gestalten etc 'goed' waren of niet. Door al deze input en door te vragen wat sommige termen ook alweer inhouden ben ik een stuk snuggerder uit deze les gekomen.

Na de lessen ben ik met mijn team de paperprototype gaan testen op onze doelgroep. Door deze test zijn wij achter een paar belangrijke aspecten gekomen om te veranderen in de game. Sommige indelingen of iconen van schermen waren onduidelijk, of zelfs de game zelf. Waarvoor speel je het? En waarom zou ik het spelen? Ik vond het tof dat mensen uit zichzelf feedback gaven. 

Onze peercoach kwam nog een praatje maken en ons tips geven. Ik vond dit heel fijn want zij heeft heel het kwarttaal voor mij duidelijker gemaakt. Bijvoorbeeld hoe het zit met tentamens, wat moet ik nu precies leren. Zelf haar email ontvangen voor verdere vragen, dit geeft mij een fijn gevoel omdat ik nu bij iemand terecht kan.
Weer een stukje meer duidelijkheid! 