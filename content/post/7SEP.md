


> Written with [StackEdit](https://stackedit.io/).
#Donderdag, 7 september
Het werkcollege over design heeft voor mij het hoorcollege veel meer inzicht gegeven. Door deze les begrijp ik hoe belangrijk en centraal je gebruiker staat voor een design en dat dit het verschil maakt in kunst en design. Verder vond ik de presentaties van de verschillende groepen over hoe zij proces zien en uitbeelden zeer interessant, door de presentaties is het weer even duidelijk hoe men zo’n verschillende kijk hebben op dingen en deze onderling snel worden gedeeld naar een mening of correct antwoord. Al in al vind ik het nu heel fijn dat er een keer theorie zeer concreet en duidelijk is uitgelegd. 

Na deze les ben ik met mijn groepje even gaan zitten om concreet naar de planning te kijken en ons concept te vormen voor de standup aankomende maandag. Wij hadden al een richting waar wij het spel in willen duwen, een soort trivia Tinder. Het concept hiervoor hebben wij samen uitgewerkt, ik vind het heel chil dat mijn groepje zo goed samen werkt en niet bang is om zijn/haar mening te delen. Na een lange brainstorm hebben we eindelijk een concrete planning, een concept maar nog geen naam voor onze app. Ik heb aan een tweede jaars student gevraagd wat zij hier van vond, waarop haar antwoord was **“fantastisch en leuk concept, ik denkt dat de leraren dit tof gaan vinden als je het goed uitwerkt en beleving er in verwerkt.”**

Wij hebben afgesproken om voor zaterdag een concrete naam te hebben en elkaar te appen als er iets te binnen schiet.Verder zijn er taken verdeeld en hebben ik en Femke de taak op ons genomen om het logo te ontwerpen voor aankomende maandag.
