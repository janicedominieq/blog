---
title: 4 september
subtitle: De eerste officiele lesweek
comments: false
---

Maandag, 
4 september


De eerste officiële les week ben ik begonnen aan de eerste design challenge samen met mijn team. Als aller eerste hebben wij in overleg met elkaar een doelgroep gekozen, dit was nog niet zo makkelijk. Wij zaten een tijdje te brainstormen over de doelgroep tot dat er een docent kwam met de tip verklein je doelgroep met als voorbeeld, gehandicapte of buitenlandse studenten. Hier hebben wij mega veel aan gehad. Na de doelgroep te kiezen is het thema in no time bedacht. Met deze twee, thema en doelgroep, zijn we verdeeld gestart aan het onderzoek. Een paar vragen in google drive gegooid waar wij besloten in te werken en ieder voor zich aan de slag gegaan aan zijn onderzoekje. 

Mijn onderzoek ging over welke musea er te vinden zijn in Rotterdam, dit is makkelijk op te zoeken dus heb ik mijn onderzoek uitgebreid. In mijn onderzoek wil ik te weten komen buiten welken musea er zijn wat hun visie en/of missie is en welke collecties zij bezitten.


